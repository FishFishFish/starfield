function Star(){
	this.x = Math.floor(Math.random() * window.innerWidth - window.innerWidth/2);
	this.y = Math.floor(Math.random() * window.innerHeight - window.innerHeight/2);
	this.z = (Math.random() * 5);//Math.floor((Math.random() * window.innerWidth));
	this.z2 = this.z;
}

var Stars = [];
var canvas = document.getElementById("StarField");
var context = canvas.getContext("2d");


$(function(){
	resizeCanvas();
	for (var i = 0; i < 200; i++) {Stars[i] = new Star();}
	draw();
});
function draw(){
	context.clearRect(0,0,canvas.width, canvas.height);	
	for(Star of Stars){
		if (Star.z <= 1){
			Star.x = Math.floor(Math.random() * window.innerWidth - window.innerWidth/2);
			Star.y = Math.floor(Math.random() * window.innerHeight - window.innerHeight/2);
			Star.z = (Math.random() * 5);
			this.z2 = this.z;
		}
		Star.z  -= 0.05;
		var tx = Star.x /Star.z+(window.innerWidth/2);
		var ty = Star.y /Star.z+(window.innerHeight/2);
		context.beginPath();
		var size=1;
		if(Star.z < 0){size = -Star.z}else{size = Star.z};
		size = (5-size)/2;
		context.arc(tx,ty,size,0,2*Math.PI);
		// star start
		// Context.font = "39px Arial";
		// var gradient = Context.createLinearGradient(0, 0, Canvas.width, 0);
		// gradient.addColorStop("0", "magenta");
		// gradient.addColorStop("0.5", "blue");
		// gradient.addColorStop("1.0", "red");
		// Context.fillStyle = gradient;
		// Context.fillText("✡",tx,ty);
		// end star
		context.fill();
		context.stroke();
		 // if(Star.z <= 1){
		 // Context.beginPath();
		 // Context.moveTo(Star.x/Star.z2+window.innerWidth/2,Star.y/Star.z2+window.innerHeight/2);
		 // Context.lineTo(tx,ty);
		 // Context.stroke();
		 
		 // }
		Star.z2 =Star.z;
	}
	  setTimeout(draw, 20);
}

window.onresize = function(event) {
	resizeCanvas();
};

function resizeCanvas(){
	canvas.width = window.innerWidth;
	canvas.height = window.innerHeight;
	context.strokeStyle="#fff";
	context.fillStyle = "#fff";
}
