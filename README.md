# Starfield #

### What is this repository for? ###

Small program that simulates a "starfield" with moving stars.

### Screenshots ###

![starfield.png](https://bitbucket.org/repo/RdbjB4/images/895225566-starfield.png)